﻿using LoginDemo.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoginDemo.API.Controllers
{
    [Authorize(Roles ="User")]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [Route("getalll")]
        [HttpGet]
        public IHttpActionResult GetAllUser()
        {
            //if (id < 10) // No error hanbdling at all:
            //{
            //    int a = 1;
            //    int b = 0;
            //    int c = 0;
            //    c = a / b; //it would cause exception.
            //}
            //else if (id < 20) // Error handling by HttpResponseException with HttpStatusCode
            //{
            //    throw new HttpResponseException(HttpStatusCode.BadRequest);
            //}
            //else if (id < 30) // Error handling by HttpResponseException with HttpResponseMessage
            //{
            //    var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
            //    {
            //        Content = new StringContent(string.Format("No Employee found with ID = {0}", 10)),
            //        ReasonPhrase = "Employee Not Found"
            //    };

            //    throw new HttpResponseException(response);
            //}
            LoginDemoDbContext context=new LoginDemoDbContext();
            return Ok(context.Users.ToList());
        }
    }
}
