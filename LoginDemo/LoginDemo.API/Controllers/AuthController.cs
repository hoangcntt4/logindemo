﻿
using LoginDemo.Core.Models;
using LoginDemo.Core.Repositories;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace LoginDemo.API.Controllers
{
    public class AuthController : ApiController
    {
        private readonly IUserRepository _userRepository;
     
        public AuthController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
    
        }

        [Route("login")]
        [HttpGet]
        public async Task<IHttpActionResult> LoginAsync(string userName, string password)
        {
            //// Validate username, password: not empty, not to short or long

            /// Check if user authenticated
            var user = await _userRepository.AuthenticateAsync(userName, password);
            if (user != null)
            {
                //// Generate JWT token
                var token = await CreateTokenAsync(user);
                return Ok(token);
            }

            return BadRequest("Username or password is incorrect");

        }
        public Object GetToken(string userId)
        {
            var key = ConfigurationManager.AppSettings["JwtKey"];

            var issuer = ConfigurationManager.AppSettings["JwtIssuer"];

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("userid", "userId"));

            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(1),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return new { data = jwt_token };
        }

        public async Task<string> CreateTokenAsync(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.GivenName, user.UserName),
                new Claim(ClaimTypes.Email, user.Email)
            };
            user.RoleNames =await _userRepository.GetAllRolesAsync(user);
            user.RoleNames.ForEach((role) =>
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            });

            var key = ConfigurationManager.AppSettings["JwtKey"];

            var issuer = ConfigurationManager.AppSettings["JwtIssuer"];

           // var audience = ConfigurationManager.AppSettings["JWTAudience"];

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            //  var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer,
                issuer,
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}