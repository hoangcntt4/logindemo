﻿
using LoginDemo.Core.Models;
using LoginDemo.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace LoginDemo.Core.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {

      
        IUserRepository UserRepository { get; }

        LoginDemoDbContext LoginDemoDbContext { get; }

        int SaveChange();

        Task<int> SaveChangeAsync();
    }
}