﻿
using LoginDemo.Core.Models;
using LoginDemo.Core.Repositories;
using System.Threading.Tasks;

namespace LoginDemo.Core.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LoginDemoDbContext context;
       
        private IUserRepository userRepository;

        public UnitOfWork(LoginDemoDbContext context)
        {
            this.context = context;
        }

       
        public LoginDemoDbContext LoginDemoDbContext => this.context;

        public IUserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new UserRepository(this.context);
                }
                return this.userRepository;
            }
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        public int SaveChange()
        {
            return this.context.SaveChanges();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await this.context.SaveChangesAsync();
        }
    }
}