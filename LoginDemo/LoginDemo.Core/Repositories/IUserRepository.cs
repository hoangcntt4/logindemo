﻿using LoginDemo.Core.Infrastructures;
using LoginDemo.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginDemo.Core.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> AuthenticateAsync(string username, string password);

        Task<User> GetByEmail(string email);
        Task<User> GetByUsername(string username);

        Task<User> RegisterAsync(User user);
        Task<User> CreateAsync(User user);
        Task<User> ChangePasswordAsync(User user, string newPassword);
        Task<string> ResetPasswordAsync(string email);

        Task<User> LockUserAsync(User user);
        Task<User> UnlockUserAsync(User user);

        Task<User> AddToRoleAsync(User user, string roleName);
        Task<User> RemoveFromRoleAsync(User user, string roleName);
        Task<List<string>> GetAllRolesAsync(User user);
    }
}
