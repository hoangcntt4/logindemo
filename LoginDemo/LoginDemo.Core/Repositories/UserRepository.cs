﻿using LoginDemo.Core.Infrastructures;
using LoginDemo.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginDemo.Core.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(LoginDemoDbContext context) : base(context)
        {
        }
        public async Task<User> AuthenticateAsync(string username, string password)
        {
            var user = context.Users.FirstOrDefault(x => x.UserName == username && x.Password == password);
            return user;
        }

        public Task<User> ChangePasswordAsync(User user, string newPassword)
        {
            throw new NotImplementedException();
        }

        public Task<User> CreateAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task<User> AddToRoleAsync(User user, string roleName)
        {
            throw new NotImplementedException();
        }
        public Task<User> RemoveFromRoleAsync(User user, string roleName)
        {
            throw new NotImplementedException();
        }
        public async Task<List<string>> GetAllRolesAsync(User user)
        {
            var listRoleId=context.UserRoles.Where(x=>x.UserId.Equals(user.Id)).Select(x=>x.RoleId).ToList();
            List<string> userNames = new List<string>();
            foreach (var roleId in listRoleId)
            {
                userNames.Add(context.Roles.FirstOrDefault(x=>x.Id.Equals(roleId)).Name);
            }

            return userNames;
        }

        public Task<User> LockUserAsync(User user)
        {
            throw new NotImplementedException();
        }

        public async Task<User> RegisterAsync(User user)
        {
            context.Users.Add(user);
            await context.SaveChangesAsync();
            return user;
        }

        public Task<string> ResetPasswordAsync(string email)
        {
            var user = context.Users.FirstOrDefault(u => u.Email == email);
            if (user != null)
            {
                string newPassword = Guid.NewGuid().ToString().Substring(0, 8).ToLower();
                return Task.FromResult(newPassword);
            }

            throw new KeyNotFoundException("Email does not exist!");
        }

        public Task<User> UnlockUserAsync(User user)
        {
            throw new NotImplementedException();
        }

        public async Task<User> GetByEmail(string email)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Email.ToLower() == email.ToLower());
            return user;
        }

        public Task<User> GetByUsername(string username)
        {
            var user = context.Users.FirstOrDefaultAsync(u => u.UserName.ToLower() == username.ToLower());
            return user;
        }
    }
}
