using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace LoginDemo.Core.Models
{
    public partial class LoginDemoDbContext : DbContext
    {
        public LoginDemoDbContext()
            : base("name=LoginDemoDbContext")
        {
        }

        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>()
                .Property(e => e.Description)
                .IsUnicode(false);
        }
    }
}
