﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginDemo.ModelView.Results
{
    public class ResponseResult<TData>
    {
        /// <summary>
        /// Create a response with sucscess result
        /// </summary>
        public ResponseResult()
        {
            this.IsSuccessed = true;
        }
        /// <summary>
        /// Create a response with faile result with errror message
        /// </summary>
        /// <param name="errorMessage"></param>
        public ResponseResult(string errorMessage)
        {
            this.IsSuccessed = false;
            this.ErrorMessage = errorMessage;
        }
        /// <summary>
        /// State of response
        /// </summary>
        public bool IsSuccessed { get; set; }



        /// <summary>
        /// Error Message
        /// </summary>
        public string ErrorMessage { get; set; }



        /// <summary>
        /// Status code
        /// </summary>
        public int StatusCode { get; set; }



        /// <summary>
        /// Data
        /// </summary>
        public TData Data { get; set; }
        /// <summary>
        /// List Data
        /// </summary>
        public List<TData> ListData { get; set; }
    }
}
