﻿using System.ComponentModel.DataAnnotations.Schema;

namespace rencode.Models
{
    public class User
    {
        public User()
        {
            RoleNames = new List<string>();
            Roles = new List<UserRole>();
        }
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; } = false;

        public bool IsLocked { get; set; } = false;
        public DateTime LockedEnd { get; set; }

        [NotMapped]
        public List<string> RoleNames { get; set; }
        public virtual IList<UserRole>? Roles { get; set; } = new List<UserRole>();

    }
}
