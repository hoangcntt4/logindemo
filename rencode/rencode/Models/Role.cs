﻿namespace rencode.Models
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; } = string.Empty;
        public virtual IList<UserRole>? Users { get; set; }
    }
}
