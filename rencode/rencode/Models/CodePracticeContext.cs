﻿
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace rencode.Models
{
    public class CodePracticeContext : DbContext
    {
        //public CodePracticeContext(DbContextOptions options) : base(options)
        //{

        //}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=LoginDb;Trusted_Connection=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>().HasOne(ur => ur.User).WithMany(u => u.Roles);
            modelBuilder.Entity<UserRole>().HasOne(ur => ur.Role).WithMany(r => r.Users);
            modelBuilder.Entity<UserRole>().HasKey(ur => new { ur.UserId, ur.RoleId });

            modelBuilder.Entity<Role>().HasData(new Role()
            {
                Id = new Guid("b7717016-1bdf-475d-9916-eb8bb22769d3"),
                Name = "User",
                Description = "Normal user in this system. Can read data only"
            });
            modelBuilder.Entity<Role>().HasData(new Role()
            {
                Id = new Guid("663ec6c3-e369-47e6-87c5-1ccb8b7dfa33"),
                Name = "Admin",
                Description = "Administrator of the system. Can create data"
            });

            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = new Guid("abb1561a-b9e9-4e1b-b89c-362c14c24ff1"),
                UserName = "user@example.com",
                Email = "user@example.com",
                EmailConfirmed = true,
                Password = "user#123"
            });            
            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = new Guid("ebad0f8c-aa01-4c06-8467-d5a892fe212f"),
                UserName = "admin@example.com",
                Email = "admin@example.com",
                EmailConfirmed = true,
                Password = "admin#123"
            });

            modelBuilder.Entity<UserRole>().HasData(new UserRole()
            {
                RoleId= new Guid("b7717016-1bdf-475d-9916-eb8bb22769d3"),
                UserId = new Guid("abb1561a-b9e9-4e1b-b89c-362c14c24ff1")
            });
            modelBuilder.Entity<UserRole>().HasData(new UserRole()
            {
                RoleId= new Guid("663ec6c3-e369-47e6-87c5-1ccb8b7dfa33"),
                UserId = new Guid("ebad0f8c-aa01-4c06-8467-d5a892fe212f")
            });
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
    }
}
